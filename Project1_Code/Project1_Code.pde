/*
	Project 1
	Name of Project: difense game
	Author:20170685 Choi Yunho
	Date:
*/

final int size = 80;
final int ballSize = 10;

ArrayList<Turret> turrets = new ArrayList<Turret>();
ArrayList<Ball> balls = new ArrayList<Ball>();
ArrayList<Monster> monsters = new ArrayList<Monster>();
ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();

int playerHp;
int money;
int wave;
int waveUpdate;

int startTime;
int waveUpdateTime;
int notEnoughMoneyTime;
int cantBuildTime;
int getMoneyTime;

boolean startGame;
boolean howToGame;
boolean buyingTurret;
boolean buyingObstacle;
boolean notEnoughMoney;
boolean cantBuild;

int sum (int n)
{
	int result = 0;
	for(int i=1; i<n+1; i++)
	{
		result += i;
	}
	return result;
}
	
void setup()
{
	// your code
	size(1000,600);
	playerHp = 30;
	money = 50;
	wave = 0;
	waveUpdate = 0;
	startTime = 0;

	startGame = false;
	howToGame = false;

	buyingTurret = false;
	buyingObstacle = false;

	notEnoughMoney = false;
	cantBuild = false;

	// sm = new FastMonster();
	// fm = new StrongMonster();

	// monsters.add(sm);
	// monsters.add(fm);

}

void draw()
{
	// your code
	if(playerHp <= 0)
	{
		background(50);

		fill(255);
		textSize(60);
		text("Game Over",width/2-175,height/2);
	}
	else if(wave >= 11)
	{
		background(50);

		fill(255);
		textSize(60);
		text("Game Clear",width/2-175,height/2);

	}
	else if (startGame)
	{
		background(25,175,75);

		textSize(25);
		fill(255);
		text("Hp : " +  playerHp, width-200, 50);
		text("Money: "+ money + "G", width-200, 100);

		fill(50);
		stroke(255);
		rectMode(CORNER);
		rect(0,0,150,50);
		fill(255);
		textSize(20);
		text("Buy Turret",25,30);

		if(mouseX < 150 && mouseY <50)
		{
			text("10G",160,30);
		}
		
		fill(50);
		stroke(255);
		rect(0,50,150,50);
		fill(255);
		textSize(20);
		text("Buy Obstacle",15,80);

		if(mouseX < 150 && mouseY <100 && mouseY>50)
		{
			text("5G",160,80);
		}

		fill(255);
		textSize(60);
		text("Wave "+ wave, width/2-125, 60);

		fill(150,50,25);
		stroke(0);
		strokeWeight(2);

		for(int i =0; i<5; i++)
		{
			for (int j=0;j<9;j++)
			{	
				rectMode(CORNER);
				rect(150+j*size,200+i*size,size,size);
			}
		}
		
		for(Ball b: balls)
		{
			if(b.visible)
			{
				b.update();
			}
		}

		for(Monster m: monsters)
		{
			m.update(balls, turrets, obstacles);
		}

		for(Obstacle o: obstacles)
		{
			o.update();
		}

		for(Turret t : turrets)
		{
			
			int timedifference = millis() - t.time;
			if (t.visible)
			{
				if(timedifference > 500)
				{
					balls.add( t.shootBall());
					t.time = millis();
				}
				t.update();
			}
			
		}
		
		if(millis() > getMoneyTime + 1000)
		{
			money += 1;
			getMoneyTime = millis();
		}


		int count = 0;
		for (Monster m : monsters)
		{
			if(m.visible) count+=1;
		}

		if(count == 0 && (sum(wave) + wave*3) == monsters.size())
		{
			wave +=1;
			startTime = millis();
		}

		
		
		if( (sum(wave) + wave*3) > monsters.size())
		{
			if(millis() > waveUpdateTime +1000)
			{
				if( int(random(0,2)) ==0 ) monsters.add(new StrongMonster());
				else monsters.add(new FastMonster());
				waveUpdateTime = millis();
			}	
		}
		

		if (notEnoughMoney)
		{	
			int timedifference = millis() - notEnoughMoneyTime;
			if(timedifference < 1000)
			{
				fill(255);
				textSize(20);
				text("NOT ENOUGH MONEY!", 25, 130);
			}
			else notEnoughMoney = false;
		}

		if(cantBuild)
		{
			int timedifference = millis() - cantBuildTime;
			if(timedifference<1000)
			{
				fill(255);
				textSize(20);
				text("You Can't Build There", mouseX, mouseY-25);
			}
		}

		if (buyingTurret)
		{
			image(loadImage("data/turret.png"),mouseX, mouseY,size,size);
		}
		if (buyingObstacle)
		{
			image(loadImage("data/obstacle.png"),mouseX, mouseY,size,size);
		}
	}
	else if (howToGame)
	{
		background(150);

		fill(50);
		rectMode(CENTER);
		rect(width/2, height/2+200, 200, 100);

		fill(0);
		textSize(50);

		text("How To Game", width/2 - 175, 50);
		textSize(20);

		text("1. You need to block monsters",100, 100);
		image(loadImage("data/strongMonster.png"),100, 110,size,size);
		text("This monster is strong, but slow",175, 175);
		image(loadImage("data/fastMonster.png"),500, 110,size,size);
		text("This monster is fast, but weak",575, 175);
		text("2. You can buy turrets and obstacles with mouse if you have enough money",100, 225);
		text("     Click the buttons on the left top",100, 250);
		text("     You can cancle buying with right mouse button",100, 275);
		text("     Turrets cost 10 gold & obstacles cost 5 gold",100, 300);
		text("3. You can check your money and Hp on the right top",100, 350);
		text("     If your hp is less than 1, Game over",100, 375);
		text("4. You can restart game with keyboard 'R' ",100, 425);

		
		fill(255);
		textSize(30);
		text("Start",width/2-32,height/2+190);
		text("Game", width/2-39, height/2 +230);
	}
	else
	{
		background(150);

		fill(50);
		rectMode(CENTER);
		rect(width/2, height/2+100, 200, 100);
		rect(width/2, height/2-100,200,100);

		fill(255);
		textSize(30);
		text("How To",width/2-50,height/2-110);
		text("Game", width/2-39, height/2 -70);
		text("Start",width/2-32,height/2+90);
		text("Game", width/2-39, height/2 +130);
	}
}

void mousePressed()
{
	if(!howToGame && !startGame)
	{
		if(mouseX>width/2-100 && mouseX<width/2+100)
		{
			if(mouseY>height/2-150 && mouseY<height/2- 50)
			{
				howToGame = true;
			}
			if(mouseY>height/2+50 && mouseY<height/2+150)
			{
				startGame = true;
				wave = 0;
				startTime = millis();
			}
		}
	}
	if(howToGame)
	{
		if(mouseX>width/2-100 && mouseX<width/2+100)
		{
			if(mouseY>height/2+100 && mouseY<height/2+300)
			{
				howToGame = false;
				startGame = true;
				wave = 0;
				startTime = millis();
			}
		}
	}

	if(startGame)
	{
		if(mouseX < 150 && mouseY <50)
		{
			if(money>=10)
			{
				buyingTurret = true;
				buyingObstacle = false;
			}
			else 
			{
				notEnoughMoney = true;
				notEnoughMoneyTime = millis();
			}
		}
		
		if(mouseX < 150 && mouseY <100 && mouseY>50)
		{
			if(money >=5)
			{
				 buyingObstacle = true;
				 buyingTurret = false;
			}
			else 
			{
				notEnoughMoney = true;
				notEnoughMoneyTime = millis();
			}
			
		}
	}
	if(mouseButton == RIGHT)
	{
		buyingTurret = false;
		buyingObstacle = false;
	}
	if(buyingTurret)
	{
		if(mouseX>150 && mouseX <870)
		{
			if(mouseY>200)
			{
				if(isEmpty(mouseX,mouseY))
				{
					Rowcol rc;
					rc = new Rowcol(mouseX, mouseY);
					turrets.add(new Turret(rc));
					money -= 10;
					buyingTurret = false;
				}
				else
				{
					cantBuild = true;
					cantBuildTime = millis();
					buyingTurret = false;
				}
				
			}
		}
	}
	if(buyingObstacle)
	{
		if(mouseX>150 && mouseX <870)
		{
			if(mouseY>200)
			{
				if(isEmpty(mouseX,mouseY))
				{
					Rowcol rc;
					rc = new Rowcol(mouseX, mouseY);
					obstacles.add(new Obstacle(rc));
					money -= 5;
					buyingObstacle = false;
				}
				else
				{
					cantBuild = true;
					cantBuildTime = millis();
					buyingObstacle = false;
				}
			}
		}
	}
}

void keyPressed() 
{
	if(key == 'r' || key == 'R')
	{
		howToGame = false;
		startGame = false;

		turrets.clear();
		monsters.clear();
		balls.clear();
		obstacles.clear();

		playerHp = 30;
		money = 50;
		wave = 1;
		
		buyingTurret = false;
		buyingObstacle = false;
	}
}

boolean isEmpty(int x, int y)
{
	Rowcol rc;
	rc = new Rowcol(x,y);
	for(Turret t :turrets)
	{
		if(t.rc.row()==rc.row() && t.rc.col() == rc.col()) 
		{
			if(t.visible) return false;
		}
	}
	for(Obstacle o :obstacles)
	{
		if(o.rc.row()==rc.row() && o.rc.col() == rc.col())
		{
			if(o.visible) return false;
		}
	}
	return true;
}

class Turret
{
	PImage img;
	Rowcol rc;
	boolean visible;
	int maxHp = 20;
	float hp;
	int time;

	Turret(Rowcol rc)
	{
		img = loadImage("data/turret.png");
		hp = maxHp;
		this.rc = rc;
		visible = true;
		time = 0;
	}

	void update()
	{
		if(visible)
		{
			imageMode(CENTER);
			image(img, rc.x() , rc.y() , size, size*img.height/img.width);

			strokeWeight(2);
			stroke(0);
			fill(255);
			rect(rc.x() - (size-20)/2 -1 , rc.y()-30 -1 , size-20 +1, 8+1);

			noStroke();
			rectMode(CORNER);
			fill(255,0,0);
			rect(rc.x() - (size-20)/2 , rc.y()-30, (size-20)*hp /maxHp, 8);
		}
		if(hp<0)
		{
			visible = false;
		}

	}
	Ball shootBall()
	{
		Ball b = new Ball(rc);
		return b;
	}

}

class Obstacle
{
	PImage img;
	Rowcol rc;
	boolean visible;
	int maxHp = 80;
	float hp;

	Obstacle(Rowcol rc)
	{
		img = loadImage("data/obstacle.png");
		hp = maxHp;
		this.rc = rc;
		visible = true;
	}
	void update()
	{
		if(visible)
		{
			imageMode(CENTER);
			image(img, rc.x() , rc.y() , size, size*img.height/img.width);

			strokeWeight(2);
			stroke(0);
			fill(255);
			rect(rc.x() - (size-20)/2 -1 , rc.y()-45 -1 , size-20 +1, 8+1);

			noStroke();
			rectMode(CORNER);
			fill(255,0,0);
			rect(rc.x() - (size-20)/2 , rc.y()-45, (size-20)*hp /maxHp, 8);
		}
		if(hp<0)
		{
			visible = false;
		}

	}
}


class Ball
{
	PVector loc;
	boolean visible;
	int speed = 10;

	Ball(Rowcol rc)
	{
		loc = new PVector(rc.x(), rc.y());
		fill(255,255,0);
		ellipseMode(CENTER);
		ellipse(loc.x, loc.y, ballSize, ballSize);
		visible = true;
	}

	void update()
	{
		loc.x -= speed;
		fill(255,255,0);
		ellipseMode(CENTER);
		ellipse(loc.x, loc.y, ballSize, ballSize);
	}

}


class Rowcol
{
	int row, col;
	
	Rowcol(int x, int y)
	{
		if(x>150 && x < width-130) row = (x-150)/size;
		if(y>200) col = (y-200)/size;
	}

	int row() {return row;}
	int col() {return col;}
	int x(){return 150 + 80*row + size/2;}
	int y(){return 200+80*col +size/2;}
}

abstract class Monster
{
	PImage img;
	PVector loc;
	boolean visible;
	int speed;
	int maxSpeed;
	int hp;
	int maxHp;
	int damageToPlayer;
	float damage;

	Monster()
	{
		visible = true;
		loc = new PVector(-size, 200 + size/2 + size * int(random(0, 5)));
	}

	void update(ArrayList<Ball> balls, ArrayList<Turret> turrets, ArrayList<Obstacle> obstacles)
	{
		loc.x += speed;

		if(visible)
		{
			imageMode(CENTER);
			image(img, loc.x , loc.y, size, size*img.height/img.width);
			strokeWeight(2);
			stroke(0);
			fill(255);
			rect(loc.x- (size-20)/2 -1 , loc.y-45 -1 , size-20 +1, 8+1);

			noStroke();
			rectMode(CORNER);
			fill(255,0,0);
			rect(loc.x - (size-20)/2 , loc.y-45, (size-20)*hp /maxHp, 8);
		}

		if(visible)
		{
			for (Ball b : balls)
			{
				if (b.loc.x < loc.x +20 && b.loc.x > loc.x && loc.y==b.loc.y)
				{
					if (b.visible)
					{
						b.visible = false;
						hp-= 5;
					}
					if (hp<= 0 )
					{
						visible = false;
					}
				}
			}
		}
		for (Turret t : turrets)
		{
			if(t.rc.x() < loc.x +size && t.rc.x() > loc.x && t.rc.y() == loc.y)
			{
				if(visible)
				{
					if(t.visible)
					{
						speed=0;
						t.hp -= damage;
					}
					else speed = maxSpeed;
				}
			}
		}
		for (Obstacle o : obstacles)
		{
			if(o.rc.x() < loc.x +size && o.rc.x() > loc.x && o.rc.y() == loc.y)
			{
				if(visible)
				{
					if(o.visible)
					{
						speed=0;
						o.hp -= damage;
					}
					else speed = maxSpeed;
				}
			}
		}
		if(visible && loc.x>900)
		{
			playerHp-=damageToPlayer;
			visible = false;
		}
	}

}

class StrongMonster extends Monster
{
	StrongMonster()
	{
		img = loadImage("data/strongMonster.png");
		maxHp = 100;
		hp = 100;
		speed = 2;
		maxSpeed = 2;
		damage = 0.1;
		damageToPlayer = 2;
	}
}

class FastMonster extends Monster
{
	FastMonster()
	{
		img = loadImage("data/fastMonster.png");
		maxHp = 50;
		hp = 50;
		speed = 4;
		maxSpeed = 4;
		damage = 0.03;
		damageToPlayer = 1;
	}
}