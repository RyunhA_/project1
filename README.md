# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](Project1_Code/)
* [Demo video](Video/)
* Description of projects and notes in README.md (this file). 
	
	

### Description of the project

* This is simple difense game
* You can see how to play game with click how to play
* If you alive in 10 waves, you can win
* I referenced hw4 and hw 5
* I planned to make farming & difense game but I change to just difense game
* I consider game format reference the game "Plants vs Zombies"

### Descirption of the code

* there are five types of screen in draw
main game screen, how to game screen, start screen, clear and game over screen
* there are four classes in this code, Turret, Ball, Monster and Obstacle
Turret is not only just turret, but also factory of Ball.
Monster is abstract class and it has two extended classes, StrongMonster and FastMonster
* I reference ball projectile of hw4 and rowcol function of hw5